#!/bin/bash

# read input arguments
dirFastq=/scratch/users/sxf279/20180214_16S/raw
dirData=$(echo $dirFastq | sed -r 's|/$||g')
dirData=$(echo $dirData | sed -r 's|/[^/]+$||g')

# 1a. generate join-PE-parameters.txt
flag=false
if $flag
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: generating qiime parameters file..."
    header="#SampleID\tBarcodeSequence\tLinkerPrimerSequence\tReversePrimer"
    header="$header\tDescription"
    echo -e $header > qiime_parameters.txt
    sampleID=$(find $dirFastq -name "*R1_001.fastq.gz")
    sampleID=$(echo $sampleID | sed -r 's/R1_001.fastq.gz//g')
    sampleID=( $(echo $sampleID | tr ' ' '\n' | sort | uniq) )
    for sample in ${sampleID[@]}
    do
	# extract SampleID
	sid=$(echo $sample | sed -r "s|.+/([^_]+)_[^/]+$|\\1|g")
	# extract foward index
	fwdIndex=$(zcat ${sample}I1_001.fastq.gz | head -n 2 | tail -n 1)
	# extract reverse index
	revIndex=$(zcat ${sample}I2_001.fastq.gz | head -n 2 | tail -n 1)
	echo -e "$sid\tNNNNNNNN\t$fwdIndex\t$revIndex\tNA" >> qiime_parameters.txt
    done
    # clean index files
    rm ${dirFastq}/*I[1-2]*fastq.gz
    echo "done"
fi

nfiles=$(find $dirFastq -name "*_R1_001.fastq.gz" | wc -l)
# echo $nfiles
# make directories for every samples and move  samples into
# their corresponding batch directory
for i in `seq 1 $nfiles`
do
    if [ ! -d $dirData/sample$i/raw ]
    then
	mkdir -p $dirData/sample$i/raw
    fi
    find $dirFastq -name "*R1_001.fastq.gz" | \
            sed -r "s/R1_/R2_/g" | \
            head -n 1 | xargs -i mv "{}" "$dirData/sample$i/raw"
   find $dirFastq -name "*R1_001.fastq.gz" | \
       head -n 1 | xargs -i mv "{}" "$dirData/sample$i/raw"
done

# modify preprocessing slurm script
sed -ri "s|^#SBATCH --array=1-.+$|#SBATCH --array=1-${nfiles}|g" \
    16Sseq.preprocess_slurm.sh
sed -ri "s|^dirData=.+|dirData=${dirData}|g" \
    16Sseq.preprocess_slurm.sh

sbatch 16Sseq.preprocess_slurm.sh
