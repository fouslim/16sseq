#!/bin/bash

# load modules
module load qiime/1.9.1
module load gsl/2.3
rdpPath=/mnt/projects/SOM_PATH_RXS745U/bin/rdp_classifier_2.2
rdpPath="$rdpPath/rdp_classifier-2.2.jar"
export RDP_JAR_PATH=${rdpPath}

# read input arguments
dirData=/scratch/users/sxf279/20180214_16S/sample58
dirFastq=$dirData/raw
genomeDir=/mnt/projects/SOM_PATH_RXS745U/genome/Greengenes

# 1b. lauch multiple_join_paired_ends.py
flag=false
if $flag
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: joining pair-end files..."
    if [ ! -d $dirData/1_joined-fastqs ]
    then
	mkdir $dirData/1_joined-fastqs
    fi
    multiple_join_paired_ends.py \
	-i $dirFastq \
	-o $dirData/1_joined-fastqs \
	--read1_indicator _R1_ \
	--read2_indicator _R2_ \
	-p join-PE-parameters.txt 2>/dev/null
    # rename assembled files
    sampleID=$(find $dirData/1_joined-fastqs -name "fastqjoin.join.fastq")
    sampleID=( $(echo $sampleID | tr ' ' '\n' | sort | uniq) )
    for sample in ${sampleID[@]}
    do
	fqFile=$(echo $sample | \
		     sed -r "s|.+/([^_]+)_[^/]+/fastqjoin.join.fastq|\\1.fastq|g")
	mv ${sample} ${dirData}/1_joined-fastqs/${fqFile}
    done
    # remove unassembled reads
    rm ${dirData}/1_joined-fastqs/*/fastqjoin.un[1-2].fastq
    rmdir ${dirData}/1_joined-fastqs/*_R1_001
    echo "done"
fi

# 1c. quality filter the joined reads
flag=false
if $flag
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: quality filter joined reads..."
    if [ ! -d $dirData/2_quality-check ]
    then
        mkdir $dirData/2_quality-check
    fi
    multiple_split_libraries_fastq.py \
	-i $dirData/1_joined-fastqs \
	-o $dirData/2_quality-check \
	-p split-libraries-parameters.txt 2>/dev/null
    echo "done"
fi

# 1d. truncate the reverse primer
flag=false
if $flag
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: truncate reverse primer..."
    if [ ! -d $dirData/3_trunc-rev-primer ]
    then
        mkdir $dirData/3_trunc-rev-primer
    fi    
    truncate_reverse_primer.py \
	-f $dirData/2_quality-check/seqs.fna \
	-m qiime_parameters.txt \
	-o $dirData/3_trunc-rev-primer 2>/dev/null
    echo "done"
fi

# 2a. create openref99_rdp_greengenes.txt

# 2b.picking OTUs using the open reference strategy
flag=false
if $flag
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: picking otus..."
    if [ ! -d $dirData/4-uclust ]
    then
        mkdir $dirData/4-uclust
    fi
    pick_open_reference_otus.py \
	-i $dirData/3_trunc-rev-primer/seqs_rev_primer_truncated.fna \
	-r $genomeDir/rep_set/99_otus.fasta \
	-o $dirData/4-uclust \
	-p openref99_rdp_greengenes.txt \
	-s 0.10 \
	--prefilter_percent_id 0.0 \
	--suppress_align_and_tree \
	-f 2>/dev/null
    echo "done"
fi

# 3a. identify chimeras
flag=true
if $flag
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: identify chimeras..."
    if [ ! -d $dirData/5-chimera-check ]
    then
        mkdir $dirData/5-chimera-check
    fi
    identify_chimeric_seqs.py \
	-i $dirData/4-uclust/rep_set.fna \
	-m usearch61 \
	-o $dirData/5-chimera-check \
	-r $genomeDir/rep_set/99_otus.fasta 2>/dev/null
    echo "done"
fi

# 3b. remove any sequences flagged as chimeras from the BIOM table
flag=true
if $flag
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: remove chimeras..."
    filter_otus_from_otu_table.py \
	-i $dirData/4-uclust/otu_table_mc2_w_tax.biom \
	-o $dirData/5-chimera-check/otu-table-mc2_wo_chim.biom \
	-e $dirData/5-chimera-check/chimeras.txt 2>/dev/null
    echo "done"
fi

# 4a. align rep_set.fna against a pre-aligned reference database
flag=true
if $flag
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: align representative reads to reference db..."
    if [ ! -d $dirData/6-pynast-aligned ]
    then
        mkdir $dirData/6-pynast-aligned
    fi
    align_seqs.py \
	-i $dirData/4-uclust/rep_set.fna \
	-o $dirData/6-pynast-aligned \
	-t $genomeDir/rep_set_aligned/99_otus.fasta \
	--alignment_method pynast \
	--pairwise_alignment_method uclust \
	--min_percent_id 70.0 2>/dev/null
    echo "done"
fi

# 4b. remove gaps from the aligned rep sets
flag=true
if $flag
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: remove gaps from reps alignment..."
    filter_alignment.py \
	-i $dirData/6-pynast-aligned/rep_set_aligned.fasta \
	-o $dirData/6-pynast-aligned \
	--suppress_lane_mask_filter 2>/dev/null
    echo "done"
fi

# 4c. add metadata to the BIOM table
flag=true
if $flag
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: add metadata to BIOM table..."
    biom add-metadata \
	-i $dirData/5-chimera-check/otu-table-mc2_wo_chim.biom \
	-o $dirData/6-pynast-aligned/otu-table.biom \
	-m qiime_parameters.txt 2>/dev/null
    echo "done"
fi

# 4d. summarize the BIOM table to assess the number of sequences/OTUs per sample
#     and store the output in a text file
flag=true
if $flag
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: summarize biom table..."
    biom summarize-table \
	 -i $dirData/6-pynast-aligned/otu-table.biom \
	 -o $dirData/6-pynast-aligned/otu-table-summary.txt 2>/dev/null
    echo "done"
fi

# 5. absolute counts
flag=true
if $flag
then
    currentDate=$(date +"%Y-%m-%d %X")
    echo -ne "$currentDate: count reads..."
    if [ ! -d $dirData/7-tax_mapping ]
    then
        mkdir $dirData/7-tax_mapping
    fi
    summarize_taxa.py \
	-i $dirData/6-pynast-aligned/otu-table.biom \
	-o $dirData/7-tax_mapping \
	-a 2>/dev/null
    echo "done"
fi
