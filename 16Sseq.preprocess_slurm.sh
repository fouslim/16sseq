#!/bin/bash

# user email address
#SBATCH --mail-user=sxf279@case.edu

# mail is sent to you when the job starts and when it terminates or aborts
#SBATCH --mail-type=END,FAIL

# name of job
#SBATCH --job-name=16Sseq.pre_%a

# standard output file
#SBATCH --output=16Sseq.pre_%a.out

# number of nodes and processors, memory required
#SBATCH --nodes=1
#SBATCH --cpus-per-task=2
#SBATCH --mem=16gb

# time requirements
#SBATCH --time=24:00:00

# create array
#SBATCH --array=58

# launch executable script
dirData=/scratch/users/sxf279/20180214_16S
bash 16Sseq.preprocess_seq.sh $dirData/sample$SLURM_ARRAY_TASK_ID
